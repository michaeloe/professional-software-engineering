# Git

Where should I store my Git projects?

A good practice is to create a `git` folder in your home directory and store projects in subdirectories depending on the remotes. The table lists the home folders of different operating systems.

| OS                 | Path to home       |
| ------------------ | ------------------ |
| Windows            | `C:\Users\<USER>\` |
| Windows (Git Bash) | `C/Users/<USER>/`  |
| Linux              | `/home/<USER>/`    |

Let us introduce the variable `$HOME` which is a virtual reference of the path listed above depending on your operating system. In the following, this variable is used to simplify the commands in the code listings. But be careful with the directory seperators, on Windows you have to use `\` instead of `/` if your are not using Git Bash for Windows.

There are many websites hosting Git platforms. The most famous Git hosting platforms are [GitLab](https://gitlab.com) and [GitHub](https://github.com).
To differentiate between those platforms (in the following named *remote*) you should put your git projects in subdirectories named by the remote.

| Platform | Path                    |
| -------- | ----------------------- |
| GitLab   | `$HOME/git/gitlab.com/` |
| GitHub   | `$HOME/git/github.com/` |

Depending on the hosting platform, additional subdirectories can be used when creating projects. You should also map those directory structures to your local directory tree.

Following this convention, you should create the folder `$HOME/git/gitlab.com/hs-karlsruhe/` as a base directory for this project for example.

Before you start using Git on the command line, setting user and email is required:

```
$ git config --global user.name Example Name
$ git config --global user.email mail@example.org
```

**Hints:**

-   Every command line command starts with `$ `, the output of the command is listed below without leading characters.

## Starting from local

When you want to start a Git project locally, you should think of where you want to publish (`git push`) it later.

For this project, we would create a folder and initialize the Git project:

```
$ mkdir -p $HOME/git/gitlab.com/hs-karlsruhe/professional-software-engineering
$ cd $HOME/git/gitlab.com/hs-karlsruhe/professional-software-engineering
$ git init
Initialized empty Git repository in $HOME/git/gitlab.com/hs-karlsruhe/professional-software-engineering/.git/
```

**Hints:**

-   With `mkdir -p` we can create new folders recursively.

In this state, you can work on your project locally and you can use almost any Git functions. But you can't push your commits until you configure a remote with a URL.

So let's create an empty `README.md` file, add it to the staging area and commit the changes.

```
$ touch README.md
$ git add README.md
$ git commit -m "Initial commit"
```

Next, add the remote `origin` with

```
$ git remote add origin https://gitlab.com/hs-karlsruhe/professional-software-engineering.git
```

**Hints:**

-   The suffix `.git` at the end of the URL is optional but it is preferred using it.

Finally, you can push your commits to the remote. At this time, you have to set the remote (only once) with `--set-upstream origin` because the branch does not already exist on the remote. So you have to link your local branch with the remote branch.

If you added the remote and you use GitLab, you can push your repository without creating a project in GitLab first. On the first push of a project, GitLab creates a private repository for you. The output of the command tells you, when a project was created.

```
$ git push --set-upstream origin master
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Writing objects: 100% (3/3), 875 bytes | 875.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
remote:
remote:
remote: The private project hs-karlsruhe/professional-software-engineering was successfully created.
remote:
remote: To configure the remote, run:
remote:   git remote add origin git@gitlab.com:hs-karlsruhe/professional-software-engineering.git
remote:
remote: To view the project, visit:
remote:   https://gitlab.com/hs-karlsruhe/professional-software-engineering
remote:
remote:
To https://gitlab.com/hs-karlsruhe/professional-software-engineering.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
```

## Starting from remote

When you want to use an already existing project hosted on a Git platform, you need to clone (`git clone`) the project to your local computer.

For this project, we would create a folder and clone the Git project (*remote*). When cloning a project, Git will always create a new folder with the name of the remote repository.

```
$ mkdir -p $HOME/git/gitlab.com/hs-karlsruhe
$ cd $HOME/git/gitlab.com/hs-karlsruhe
$ git clone https://gitlab.com/hs-karlsruhe/professional-software-engineering.git
```

**Hints:**

-   The suffix `.git` at the end of the URL is optional but it is preferred using it.
-   The folder `professional-software-engineering` is automatically created

When you clone a project, the remote of the default branch (usually `master`) is always set to `origin` so you do not have to explicitely use `git push origin <branch>` but only `git push`.

## Forking Workflow

This tutorial is based on <https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow> where you also can find some more explanations.

Almost every open source project based on Git uses the Forking Workflow. With this workflow, it is possible, to contribute to projects without having direct access to the official repositories. When forking a project, the official repository is cloned into a separate repository where the one who did the fork have unlimited access to.

The steps of the tutorial are enriched with an example. In this example, the official repository is <https://gitlab.com/hs-karlsruhe/professional-software-engineering> where you do not have write permissions. The fork should be created in the namepsace [https://gitlab.com/hs-karlsruhe/<semester>/<HskaUser>](https://gitlab.com/hs-karlsruhe/<semester>/<HskaUser>) and the URL of the project will be [https://gitlab.com/hs-karlsruhe/<semester>/<HskaUser>/professional-software-engineering](https://gitlab.com/hs-karlsruhe/<semester>/<HskaUser>/professional-software-engineering).

**Note:** You are not allowed to push to the official repository, you always have to push to your personal fork!

1.  A developer 'forks' an 'official' server-side repository. This creates their own server-side copy.

    Open <https://gitlab.com/hs-karlsruhe/professional-software-engineering/-/forks/new> to fork this project. Choose the namespace `hs-karlsruhe/<semester>/<HskaUser>`. In the following we will use `hs-karlsruhe/ss2020/must0004`.

1.  The new server-side copy is cloned to their local system.

    ```
    git clone https://gitlab.com/hs-karlsruhe/ss2020/must0004/professional-software-engineering.git
    ```

    When cloning, the remote `origin` is the fork remote and not the official repository.

1.  A Git remote path for the 'official' repository is added to the local clone.

    The official repository should be called `upstream`.

    **Note:** The remote `origin` is always the default remote. If you want to use the remote `upstream` you have to explicitely name it.

    ```
    git remote add upstream https://gitlab.com/hs-karlsruhe/professional-software-engineering.git
    ```

    To see the remote settings, run

    ```
    git remote -v
    ```

    and you will see the output

    ```
    origin   https://gitlab.com/hs-karlsruhe/ss2020/must0004/professional-software-engineering.git (fetch)
    origin   https://gitlab.com/hs-karlsruhe/ss2020/must0004/professional-software-engineering.git (push)
    upstream   https://gitlab.com/hs-karlsruhe/professional-software-engineering.git (fetch)
    upstream   https://gitlab.com/hs-karlsruhe/professional-software-engineering.git (push)
    ```

    Now fetch all information of the added remote `upstream` with

    ```
    git fetch upstream
    ```

1.  A new local feature branch is created.

    In the following, we will name this feature branch `new-feature`.

    ```
    git checkout -b new-feature
    ```

1.  The developer makes changes on the new branch.

1.  New commits are created for the changes.

    ```
    git add --all
    git commit -m "commit message"
    ```

1.  The branch gets pushed to the developer's own server-side copy.

    If the branch is not yet pushed, use

    ```
    git push --set-upstream origin new-feature
    ```

    to create the branch on the remote `origin` and link that branch with your local one.

    For further pushes, you can just use `git push` if you checked out the branch you want to push.

1.  The developer opens a pull request from the new branch to the 'official' repository.

    To open a merge request (= pull request), go to your projects merge request page (in this case <https://gitlab.com/hs-karlsruhe/ss2020/must0004/professional-software-engineering/-/merge_requests>) and then click on "New merge request". Select `hs-karlsruhe/ss2020/must0004/professional-software-engineering` as source project and `new-feature` as source branch. For the target, choose the upstream `hs-karlsruhe/professional-software-engineering` as project and `master` as branch. Click "Compare branches and continue" and add some information if desired. To finish your merge request, press "Submit merge request".

    When the official repositroy is very active (many commits in the master during a week), it is likely to get merge conflicts from the time a merge request was opened until the review is in progress. In this case, check [Dealing with merge/rebase conflicts](#dealing-with-mergerebase-conflicts).

1.  The pull request gets approved for merge and is merged into the original server-side repository

    This step must be done by the code owner. You have to wait until he sends some feedback or he merged your request.

At this point, the instructions of the Atlassian tutorial ends. But in practice, developers often continue submitting code for the same project. In this case, you should always synchronize your fork repository with the latest version of the upstream.

### Synchronize the fork

1.  Fetch all changes from the upstream

    ```
    git fetch upstream
    ```

1.  Checkout the master branch, because you should always branch from your master branch.

    ```
    git checkout master
    ```

1.  Rebase your fork master branch with all changes of the upstream master branch.

    ```
    git rebase upstream/master
    ```

    This is very important because if you open a merge request with an outdated commit history, it is very likely to get merge conflicts! In the case of a merge conflict, check [Handling merge and rebase conflicts](#handling-merge-and-rebase-conflicts).

1.  Push the master branch to update the fork repository.

    ```
    git push master
    ```

Always repeat these steps before you start working on a new feature when the upstream master history is newer than the master history of the fork.

### Handling merge and rebase conflicts

A merge conflict appears when multiple code changes in different branches at the same position were done.

Let's construct an example to demonstrate why merge conflicts apear and how they can be handled correctly. There are the three branches `master`, `add-name-anna` and `add-name-oliver` of the same repository `origin`. The master branch contains a headline and one name in a list. Anna and Oliver got the task to add their names on the list and merge them to the master branch. So they both created a branch from the master at the same time.

#### Initial situation

**Branch `master`**

```
$ cat README.md
# List of participants

-   Stephan Müller
```

```
$ git log
commit 22435ad60679181877ec8b5b330886a2c9bfa179 (HEAD -> master)
Author: Stephan Müller
Date:   Sat May 30 13:55:22 2020 +0200

    Add list of participants
```

**Branch `add-name-anna`**

```
$ cat README.md
# List of participants

-   Stephan Müller
-   Anna Nass
```

```
$ git log
commit 23934978624baedc8b6fe75a34405d6da9c6f2f2 (HEAD -> add-name-anna)
Author: Anna Nass
Date:   Sat May 30 13:56:54 2020 +0200

    Add name Anna

commit 22435ad60679181877ec8b5b330886a2c9bfa179 (master)
Author: Stephan Müller
Date:   Sat May 30 13:55:22 2020 +0200

    Add list of participants
```

**Branch `add-name-oliver`**

```
$ cat README.md
# List of participants

-   Stephan Müller
-   Oliver Himmel
```

```
$ git log
commit bf9a8806556e9036b1c1b5f401255ac9143a0e05 (HEAD -> add-name-oliver)
Author: Oliver Himmel
Date:   Sat May 30 13:58:10 2020 +0200

    Add name Oliver

commit 22435ad60679181877ec8b5b330886a2c9bfa179 (master)
Author: Stephan Müller
Date:   Sat May 30 13:55:22 2020 +0200

    Add list of participants
```

#### Anna merges her changes in master

At this time, Anna merges her changes back to the master branch. There is no conflict and the pointer to the master branch is moved to Anna's commit.

```
$ git checkout master
Switched to branch 'master'

$ git merge add-name-anna
Updating 22435ad..2393497
Fast-forward
 README.md | 1 +
 1 file changed, 1 insertion(+)

$ git log
commit 23934978624baedc8b6fe75a34405d6da9c6f2f2 (HEAD -> master, add-name-anna)
Author: Anna Nass
Date:   Sat May 30 13:56:54 2020 +0200

    Add name Anna

commit 22435ad60679181877ec8b5b330886a2c9bfa179
Author: Stephan Müller
Date:   Sat May 30 13:55:22 2020 +0200

    Add list of participants

$ cat README.md
# List of participants

-   Stephan Müller
-   Anna Nass
```

At this point, `README.md` has the same content in Anna's branch and in the master branch.

#### Oliver merges his changes in master

Now, Oliver also decided to merge his changes into the master. But because he changed the exact same line in the list, he gets a merge conflict.

There exists two strategies to merge those changes into master, the *merge* and the *rebase* strategy.

##### Merge strategy

When using the merge strategy, you first checkout the target branch and then merge the feature branch into master.

```
$ git checkout master
Switched to branch 'master'

$ git merge add-name-oliver
Auto-merging README.md
CONFLICT (content): Merge conflict in README.md
Recorded preimage for 'README.md'
Automatic merge failed; fix conflicts and then commit the result.

$ cat README.md
# List of participants

-   Stephan Müller
<<<<<<< HEAD
-   Anna Nass
=======
-   Oliver Himmel
>>>>>>> add-name-oliver

$ git status
On branch master
You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Unmerged paths:
  (use "git add <file>..." to mark resolution)
	both modified:   README.md

no changes added to commit (use "git add" and/or "git commit -a")
```

Git writes information about the conflict directly in the affected files. First, the changes of the current branch are listed (`<<<<<<< HEAD`) and then the changes of the branch that should be merged are listed (`>>>>>>> add-name-oliver`). Both changes are seperated by the delimiter `=======`.

There are different options to resolve the conflict.

-   **Resolve as ours**

    Apply the changes of branch `add-name-oliver` and discard the changes from the `master` branch.

    ```
    $ cat README.md
    # List of participants

    -   Stephan Müller
    -   Oliver Himmel
    ```

-   **Resolve as theirs**

    Apply the changes of branch `master` and discard the changes from the `add-name-oliver` branch.

    ```
    $ cat README.md
    # List of participants

    -   Stephan Müller
    -   Anna Nass
    ```

-   **Resolve as ours than theirs**

    Apply the changes of branch `add-name-oliver` and then the changes from the `master` branch.

    ```
    $ cat README.md
    # List of participants

    -   Stephan Müller
    -   Oliver Himmel
    -   Anna Nass
    ```

-   **Resolve as theirs than ours**

    Apply the changes of branch `master` and then the changes from the `add-name-oliver` branch.

    ```
    $ cat README.md
    # List of participants

    -   Stephan Müller
    -   Anna Nass
    -   Oliver Himmel
    ```

We choose the last option to continue. Before adding any changes, make sure, that the merge conflict is completely resolved and all Git merge placeholders (`<<<<<<<`, `>>>>>>>` and `=======`) are removed.

Then you must add the changes and commit them. When you use option `--no-edit` for committing the changes, Git uses the pre-filled merge message with a summary of all files that had a merge conflict.

```
$ git add README.md

$ git commit --no-edit
Recorded resolution for 'README.md'.
[master 18506cc] Merge branch 'add-name-oliver'

$ git log
commit 18506cc311b3c4840428e86a91840ab17f78946d (HEAD -> master)
Merge: 2393497 bf9a880
Author: Oliver Himmel
Date:   Sat May 30 15:33:10 2020 +0200

    Merge branch 'add-name-oliver'

    # Conflicts:
    #       README.md

commit bf9a8806556e9036b1c1b5f401255ac9143a0e05 (add-name-oliver)
Author: Oliver Himmel
Date:   Sat May 30 13:58:10 2020 +0200

    Add name Oliver

commit 23934978624baedc8b6fe75a34405d6da9c6f2f2 (add-name-anna)
Author: Anna Nass
Date:   Sat May 30 13:56:54 2020 +0200

    Add name Anna

commit 22435ad60679181877ec8b5b330886a2c9bfa179
Author: Stephan Müller
Date:   Sat May 30 13:55:22 2020 +0200

    Add list of participants
```

When looking at the commit history, you can see the commit `Merge branch 'add-name-oliver'` besides the initial commit `Add name Oliver`.

##### Rebase strategy

When using the rebase strategy, you first checkout the feature branch, then rebase it onto the master branch and finally merge the feature branch into the master.

```
$ git rebase master   
First, rewinding head to replay your work on top of it...
Applying: Add name Oliver
Using index info to reconstruct a base tree...
M	README.md
Falling back to patching base and 3-way merge...
Auto-merging README.md
CONFLICT (content): Merge conflict in README.md
error: Failed to merge in the changes.
Patch failed at 0001 Add name Oliver
hint: Use 'git am --show-current-patch' to see the failed patch
Resolve all conflicts manually, mark them as resolved with
"git add/rm <conflicted_files>", then run "git rebase --continue".
You can instead skip this commit: run "git rebase --skip".
To abort and get back to the state before "git rebase", run "git rebase --abort".

$ cat README.md
# List of participants

-   Stephan Müller
<<<<<<< HEAD
-   Anna Nass
=======
-   Oliver Himmel
>>>>>>> Add name Oliver
```

The resulted rebase conflict is exactly the same like with the merge strategy. So you can also use one of the four given options to resolve a conflict.

We also use the fourth option to resolve the conflict. When we have done our changes, we can continue with the rebase.

```
$ git add README.md

$ git rebase --continue
Applying: Add name Oliver
```

After we continued the rebase, we can merge our changes into master without having a merge conflict.

```
$ git checkout master
Switched to branch 'master'

$ git merge add-name-oliver
Updating 2393497..23c9c07
Fast-forward
 README.md | 1 +
 1 file changed, 1 insertion(+)

$ git log
commit 23c9c0767fba81575ef1336100627901fb9cc289 (HEAD -> master, add-name-oliver)
Author: Oliver Himmel
Date:   Sat May 30 13:58:10 2020 +0200

    Add name Oliver

commit 23934978624baedc8b6fe75a34405d6da9c6f2f2 (add-name-anna)
Author: Anna Nass
Date:   Sat May 30 13:56:54 2020 +0200

    Add name Anna

commit 22435ad60679181877ec8b5b330886a2c9bfa179
Author: Stephan Müller
Date:   Sat May 30 13:55:22 2020 +0200

    Add list of participants
```

When you compare the result with the merge strategy, you can see that there is no merge commit because the conflict was resolved before the merge.

**Attention**

The rebase changed the order of commits in the branch `add-name-oliver`. For that reason, you have to force push your changes to your branch when you want to publish them with `git push -f`. But be careful, you might override commits of others when the pushed some changes in the same branch in the meantime!
