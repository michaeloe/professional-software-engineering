# Python

Download Python from <https://www.python.org/downloads/> and install it.

You can check for a successful installation with command

```
python --version
```

which should print the downloaded version.

## Pip

Pip is the package manager for installing Python dependencies. It is installed with Python by default.

To install a new package run

```
pip install <package>
```

and to update an existing package to the latest version run

```
pip install --upgrade <package>
```

## Virtual environment

Native Python installations only support one version of a package at a time. If a package is added to the native Python installation, all Python programs can use it. At first this might sound nice because it saves efforts but for developers it is a real nightmare. Why? Let's see.

A developer

-   wants to use different versions of packages for different tools
-   wants to install and test packages without breaking functionalities of other tools
-   wants to easily setup a developer environment for new projects
-   needs a constant development environment for collaboration (not "works on my machine" like)

To adress all of the points above, Python introduced sandboxes which are known as *virtual environments*. Especially on Linux the usage of virtual environments is essential because many operating system tools are implemented in Python (e.g. package manager apt). When installing or deleting Python dependencies it can result in a disfunctional operating system.

Technically, a virtual environment is a clone of a native Python installation which is copied to a custom folder (typically in folder `.venv` or `venv` in a project's root).

## Pipenv

Pip does not nativly support virtual environments. So there is a need of another tool that combines the power of pip and virtual envs. Pipenv is a feature rich tool that is developed for exactly that use case.

Typically, a distinction is made between two types of dependencies. On the one side there are dependencies needed only for development purposes (especially for testing or compiling) and on the other side there are dependencies needed at runtime. When delivering a software, only the minimum required set of dependencies should be shipped to minimize the resource consumption and complexity.

To honor this development pattern, pipenv differentiates between the development packages (`dev-packages`) and runtime packages (`packages`).

### Setup pipenv (only once)

Before you can work with pipenv, you have to do some setup steps for your operating system.

1.  Install pipenv

    ```
    $ pip install pipenv
    ```

    When using Linux, you probably must run this command with `sudo`.

1.  Check pipenv installation

    ```
    $ pipenv --version
    ```

1.  Set environment variable `PIPENV_VENV_IN_PROJECT=1`

    As mentioned above, the virtual environment typically is stored in a `.venv` or `venv` folder. By default, pipenv does not create that folder in the current project but uses another path outside the projects for all pipenv instances. But it is a lot easier to work on projects where the `.venv` folder is placed inside the project. We can enforce pipenv to create the `.venv` folder inside the project by setting the environment variable.

    **Windows**

    *Hint*: When using Git Bash, you have to use the command provided in the Linux section.

    ```
    $ set PIPENV_VENV_IN_PROJECT 1
    ```

    This variable is only valid for the current shell context. To permanently add the environment variable, you can set it in the global settings according to <https://docs.oracle.com/en/database/oracle/r-enterprise/1.5.1/oread/creating-and-modifying-environment-variables-on-windows.html#GUID-DD6F9982-60D5-48F6-8270-A27EC53807D0>.

    **Linux**

    ```
    $ export PIPENV_VENV_IN_PROJECT=1
    ```

    This variable is only valid for the current shell context. To permanently add the environment variable, add the line

    ```
    PIPENV_VENV_IN_PROJECT=1
    ```

    to the files `$HOME/.pam_environment` and `$HOME/.profile`.

### Initialize a new project with pipenv (for every new project)

Before you can start developing a python project, you have to initialize the project with pipenv. Once your project is ready, you can install the required dependencies.

To initialize a new project, execute the following tasks:

1.  Create a new folder for the project (or use an already existing one)

    ```
    $ mkdir -p $HOME/gitlab.com/hs-karlsruhe/my-first-pipenv-project
    ```

1.  Navigate to the folder

    ```
    $ cd $HOME/gitlab.com/hs-karlsruhe/my-first-pipenv-project
    ```

1.  Initialize pipenv project in the existing folder

    This step will create a new virtual environment in folder `.venv` in the project.

    ```
    $ pipenv --python 3
    Creating a virtualenv for this project…
    Pipfile: /home/sm/git/gitlab.com/hs-karlsruhe/my-first-pipenv-project/Pipfile
    Using /usr/bin/python3.8 (3.8.2) to create virtualenv…
    ⠏ Creating virtual environment...created virtual environment CPython3.8.2.final.0-64 in 635ms
     creator CPython3Posix(dest=/home/sm/git/gitlab.com/hs-karlsruhe/my-first-pipenv-project/.venv, clear=False, global=False)
     seeder FromAppData(download=False, CacheControl=latest, pyparsing=latest, requests=latest, pytoml=latest, pkg_resources=latest, idna=latest, lockfile=latest, certifi=latest, pip=latest, distlib=latest, retrying=latest, urllib3=latest, pep517=latest, html5lib=latest, msgpack=latest, wheel=latest, appdirs=latest, progress=latest, packaging=latest, distro=latest, six=latest, setuptools=latest, contextlib2=latest, webencodings=latest, chardet=latest, colorama=latest, ipaddr=latest, via=copy, app_data_dir=/home/sm/.local/share/virtualenv/seed-app-data/v1.0.1.debian)
     activators BashActivator,CShellActivator,FishActivator,PowerShellActivator,PythonActivator,XonshActivator

    ✔ Successfully created virtual environment!
    Virtualenv location: /home/sm/git/gitlab.com/hs-karlsruhe/my-first-pipenv-project/.venv
    Creating a Pipfile for this project…
    ```

    When you check the files and folders of your project, you will see the folder `.venv` and the file `Pipfile`.

    ```
    $ ls -a
    .  ..  Pipfile  .venv
    ```

    The content of `Pipfile` will look like this:

    ```
    $ cat Pipfile
    [[source]]
    name = "pypi"
    url = "https://pypi.org/simple"
    verify_ssl = true

    [dev-packages]

    [packages]

    [requires]
    python_version = "3.8"
    ```

    So you can see some information about the source of pip packages, a (empty) list of `dev-packages` and `packages` and also the python version you choose.

1.  Install pip development packages `pytest`, `pytest-cov` and `pylint`

    These packages are strongly recommended for every python project to prepare it for testing and linting.

    ```
    $ pipenv install --dev pytest pytest-cov pylint
    Installing pytest…
    Adding pytest to Pipfile's [dev-packages]…
    ✔ Installation Succeeded
    Installing pytest-cov…
    Adding pytest-cov to Pipfile's [dev-packages]…
    ✔ Installation Succeeded
    Installing pylint…
    Adding pylint to Pipfile's [dev-packages]…
    ✔ Installation Succeeded
    Pipfile.lock not found, creating…
    Locking [dev-packages] dependencies…
    ✔ Success!
    Locking [packages] dependencies…
    Updated Pipfile.lock (86692b)!
    Installing dependencies from Pipfile.lock (86692b)…
     🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 18/18 — 00:00:07
    To activate this project's virtualenv, run pipenv shell.
    Alternatively, run a command inside the virtualenv with pipenv run.
    ```

    After the first installation of a new package, pipenv creates the new file `Pipfile.lock`.

    ```
    $ ls -a
    .  ..  Pipfile  Pipfile.lock  .venv
    ```

    In this file, all recurse dependencies are listed and saved with there installed package version.

### Initialize an existing project with pipenv (for every existing project)

When working with an existing pipenv project, all the initialization steps above are already executed. To work with an existing project is as simple as going into the project directory and execute pipenv install command.

```
$ cd $HOME/gitlab.com/hs-karlsruhe/existing-pipenv-project
$ pipenv install --dev
```

With this command all `dev-packages` and `packages` are installed based on the entries in `Pipfile` and `Pipfile.lock`.

### Installing runtime dependencies

To install runtime dependencies, you must run `pipenv install` without option `--dev`.

```
$ pipenv install <name-of-package>
```

### Running commands inside virtual environment

By default, the Python installation of your operating system is used when running `python` command.

There are two options to run commands from inside the virtual environment. To activate this project's virtualenv permanently for the current shell, run `pipenv shell`. Alternatively, run a command once inside the virtualenv with `pipenv run`.

## Python project structure

```
├── .git/
├── .venv/
├── <project_name>/
│ ├── __init__.py
│ ..  *.py
│ └── <project_name>.py
├── deploy/
├── docs/
├── tests/
│ ├── __init__.py
│ ..  test_*.py
│ └── test_<project_name>.py
├── .gitignore
├── .gitlab-ci.yml
├── Pipfile
├── Pipfile.lock
└── README.md
```

### Git

A good `.gitignore` template for Python can be found at <https://www.gitignore.io/api/python>. Additionally, you should add the entries `.venv/` to exclude the virtual environment and `.idea/` to exclude Jetbrains specific project settings.

### Pipenv

As you already know, pipenv creates the two files `Pipfile` and `Pipfile.lock` as well as the virtual environment in `.venv/`.

### Packages and modules

All relevant information about packages and modules can be found at <https://docs.python.org/3/tutorial/modules.html>. But not all information is relevant for this lecture. Here is a list of the sections you should really understand (if subitems are not included in the list explicitly, they are not relevant but only the text until the following subitem):

-   [6. Modules](https://docs.python.org/3/tutorial/modules.html#modules)
-   [6.1 More on Modules](https://docs.python.org/3/tutorial/modules.html#more-on-modules)
-   [6.1.1. Executing modules as scripts](https://docs.python.org/3/tutorial/modules.html#executing-modules-as-scripts)
-   [6.4 Packages](https://docs.python.org/3/tutorial/modules.html#packages)

All files needed at runtime are located in the package folder `<project_name>/`. Python does not allow folder or file names with hyphens (`-`), so you have to replace all hyphens with underscores (`_`).

Every file in the package folder (`*.py`) should have a corresponding test file (`test_*.py`) in the `tests/` folder.

### Documentation

Documentation about the source code is done directly in the related source files. There are Python packages, that generate documentation sites out of the source code documentation. Usually, the files required for automaticlly generate the sites are stored in the `docs/` folder. You can also add additional information (such as a user manual) which are also placed in that folder.

The documentation about the source code as also known as "docstrings".

#### Docstrings

> A docstring is a string literal that occurs as the first statement in a module, function, class, or method definition.
>
> -- <cite><https://www.python.org/dev/peps/pep-0257/#id15></cite>

Python describes the usage of docstrings in [PEP-0257](https://www.python.org/dev/peps/pep-0257/) (Python Enhancement Proposals).

Docstrings are encapsulated with triple quotes `"""`.

**Module definition**

```python
"""A very minimalistic web server."""

def webserver():
    serve()
```

**Function definition**

without parameters

```python
def webserver():
    """Serves a hello world page."""
    serve()
```

with parameters

```python
def sum(operand1, operand2):
    """Calculate sum of two operands.

    :param operand1: First operand
    :param operand2: Second operand
    :return: Sum of two operands
    """
    return operand1 + operand2
```

**Class definition**

```python
class Math:
    """Collection of math operations."""

    def sum(self):
        pass
```

### Continous Integration

The `.gitlab-ci.yml` file defines jobs for GitLab CI such as executing linting checks or test cases. The project <https://gitlab.com/hs-karlsruhe/ci-templates> provides generic templates which can be referenced in your own CI file. Have a look at the readme of this project to see what content you have to write in the CI file.

### Deployment

If a continous delivery is setup for the project, all scripts needed for deployment should be placed in folder `deploy/`.

## Executing Python files

Python files can be executed by calling `python` with the name of the script as parameter.

```
python test.py
```

## Linting

> Pylint is a tool that checks for errors in Python code, tries to enforce a coding standard and looks for code smells. It can also look for certain type errors, it can recommend suggestions about how particular blocks can be refactored and can offer you details about the code's complexity.
>
> Pylint will display a number of messages as it analyzes the code and it can also be used for displaying some statistics about the number of warnings and errors found in different files. The messages are classified under various categories such as errors and warnings.
>
> Last but not least, the code is given an overall mark, based on the number and severity of the warnings and errors.
>
> -- <cite><http://pylint.pycqa.org/en/latest/intro.html></cite>

The Pylint rules can be configurated by providing a config file. Because the default rules are a bit too restrictive, you should apply the ruleset from <https://gitlab.com/hs-karlsruhe/ci-templates/-/blob/master/pylintrc>. Download this file and place it in the root of your project. Pylint will automatically use this file as ruleset (more detailed information see at <http://pylint.pycqa.org/en/latest/user_guide/run.html#command-line-options>).

Run

```
$ pylint **/**.py
```

to lint all your files in your project folder.

## Testing

Running tests in Python is as easy as executing

```
pytest
```

on the command line. Or with additional code coverage output, run

```
pytest --cov=<package-name>
```

Everything about testing can be found at <https://docs.pytest.org/en/latest/index.html>.

Tests files are placed in the `tests/` folder. Only files prefixed with `test_` are recognized py Pytest for test evaluations. You should use the original file name you want to test after the prefix. So when you named a file `math.py`, the test file should be `test_math.py`.

If you want to use functions of your package and modules, you must initiate your `tests/` folder also as a package by adding an empty `__init__.py` file.

Like test files, test functions must also start with prefix `test_` so that Pytest recognizes it as test function.

`assert` is a keyword for evaluating a condition. If any assertion in the test function is false, the test will fail.

```python
def sum(operand1, operand2):
    return operand1 + operand2


def test_sum():
    assert sum(1, 3) == 4
```

## Logging

Using `print` for logging is bad practice (except some use cases like command line tools) because

-   `print()` does not have a context (no information where the output is generated, no information about the importance)
-   output resulting from `print()` is always printed, there is no switch to stop it
-   if `print()` is used frequently, the output gets very messy
-   dependencies could be unusable

The use of the logging framework is highly recommended because of its advantages.

-   Loggers record events during program execution
-   Events contains a level, a message and additional meta information (which are automatically set by the logging framework)
-   Logging is very helpful for debugging
-   Every Python module can (and should) use a separate logger instance so they are distinguishable
-   Loggers are highly customizable
    -   Formatting the output (file name, module, function, line of code, level, message, timestamp, process, thread and so on)
    -   Filter for filtering only desired log events (e.g. by logger names)
    -   Type of output (terminal, file, external services etc.)

Python delivers a logging framework in its standard library. While this section contains only the most important information about logging in Python, you can get more detailed information at <https://docs.python.org/3/howto/logging.html>.

Here is an example that uses the root logger as well as a separate logger for the current module.

```python
import logging

# See https://docs.python.org/3/howto/logging.html#changing-the-format-of-displayed-messages
# how to customize log message format
logging.basicConfig(level=logging.INFO, format=logging.BASIC_FORMAT)

# Set the logger name to the name of the module
logger = logging.getLogger(__name__)


# See https://docs.python.org/3/tutorial/modules.html#executing-modules-as-scripts
# why this is used
if __name__ == "__main__":

    # Log to root logger
    logging.info("First message")

    # Log to logger instance
    logger.debug("My debug message")
    logger.info("Hello World")
    logger.warning("Terminating")
```

When executing the file, the output is:

```
INFO:root:First message
INFO:__main__:Hello World
WARNING:__main__:Terminating
```

The message `My debug message` is not being printed because the logger was configured to only print messages up to level `INFO`. When you want to see the debug messages, you have to change the level to `DEBUG`.

## Webservice

We will use [Flask](https://flask.palletsprojects.com/en/1.1.x/) to develop web services. Install `flask` as a runtime package into your pipenv project to start building a webserver.

This code snippet provides a very simple flask web app.

```python
from flask import Flask

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello, World!'


if __name__ == "__main__":
    app.run(debug=False, host="0.0.0.0", port=8080)
```
